import express from 'express';
import cors from 'cors';
import fs from 'fs';

const lessons = JSON.parse(fs.readFileSync('lessons.json', 'utf-8'));

const app = express();

app.use(cors());

app.get('/', (req, res) => {
    console.log('GET /');
    let remainingLessons = [];
    for (let lesson of lessons) {
        if (lesson > Date.now() / 1000) remainingLessons.push(lesson);
    }
    res.status(200).json({
        remainingLessons,
        amount: remainingLessons.length
    });
});

app.listen(1789, () => {
    console.log("Listening on port 1789!");
})